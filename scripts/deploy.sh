#!/bin/bash
# SCRIPT TO AUTOMATE MY UBUNTU DESKTOP INSTALLATION
# Minimal Ubuntu Desktop as base installation
# INTERNET CONNECTION REQUIRED TO SUCCESSFULLY EXECUTE THIS SCRIPT

# Set BASH to quit script and exit on errors:
set -eu

# Install essential applications
cd ~
sudo apt install -yy git curl gdebi fping preload node-typescript geany neofetch tmux byobu ranger pavucontrol gnome-tweaks piper remmina filezilla sane xsane xsane-common printer-driver-escpr traceroute
sudo snap install bitwarden telegram-desktop bpytop nmap
sudo snap connect nmap:network-control
sudo snap install authy --beta

# Download Terminology's snap and set it as default for GNOME 3.38
sudo snap install terminology --classic
gsettings set org.gnome.desktop.default-applications.terminal exec terminology

# AppImage launcher/integration
sudo add-apt-repository ppa:appimagelauncher-team/stable
sudo apt install -yy appimagelauncher

# Install nice to have applications
sudo apt install -yy qbittorrent steam gnome-weather gnome-clocks gnome-sushi lollypop
sudo snap install spotify discord gimp krita nimblenote youtube-dlr

# Lutris for gaming
sudo add-apt-repository ppa:lutris-team/lutris
sudo apt install -yy lutris

# WINE for... stuff
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'
sudo apt install -yy --install-recommends winehq-devel

# Optional if you do not install Pop! Shell use uLauncher as your preferred app launcher
#wget https://github.com/Ulauncher/Ulauncher/releases/download/5.8.1/ulauncher_5.8.1_all.deb
#sudo gdebi -n ./ulauncher_5.8.1_all.deb
#rm ./ulauncher_5.8.1_all.deb

# Quality of life improvements
sudo apt purge -yy irqbalance
sudo apt install -yy libcanberra-gtk-module libsdl2-2.0-0 gnome-shell-extensions gnome-shell-extension-gsconnect gnome-shell-extension-gamemode
sudo bash -c "echo 'vm.swappiness = 10' >> /etc/sysctl.conf"
sudo bash -c "echo 'vm.max_map_count = 16777216' >> /etc/sysctl.conf"
sudo bash -c "echo 'fs.suid_dumpable = 0' >> /etc/sysctl.conf"
# Useful to run "Wastelands 2"
sudo sed -i.bak 's/#DefaultLimitNOFILE=1024:524288/DefaultLimitNOFILE=524288/g' /etc/systemd/system.conf
sudo sed -i.bak 's/#DefaultLimitNOFILE=/DefaultLimitNOFILE=524288/g' /etc/systemd/user.conf
gsettings set org.gnome.mutter center-new-windows 'true'
gsettings set org.gnome.shell.extensions.dash-to-dock show-mounts false
gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'
gsettings set org.gnome.shell.extensions.dash-to-dock autohide true
#gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false
#gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false
#gsettings set org.gnome.shell.extensions.dash-to-dock autohide-in-fullscreen true
#gsettings set org.gnome.shell.extensions.dash-to-dock intellihide true
#gsettings set org.gnome.shell.extensions.dash-to-dock intellihide-mode ALL_WINDOWS
# Optional if not using Pop! shell... or if you want to have both
#git clone https://github.com/material-shell/material-shell.git ~/.local/share/gnome-shell/extensions/material-shell@papyelgringo

# Enable some Gnome Extensions
gnome-extensions enable user-theme@gnome-shell-extensions.gcampax.github.com
gnome-extensions enable gsconnect@andyholmes.github.io
#gnome-extensions enable drive-menu@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable places-menu@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable openweather-extension@jenslody.de
#gnome-extensions enable windowsNavigator@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable workspace-indicator@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable apps-menu@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable auto-move-windows@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable window-list@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable screenshot-window-sizer@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable horizontal-workspaces@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable launch-new-instance@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable native-window-placement@gnome-shell-extensions.gcampax.github.com
#gnome-extensions enable desktop-icons@csoriano
#gnome-extensions enable ubuntu-appindicators@ubuntu.com
#gnome-extensions enable ubuntu-dock@ubuntu.com
#gnome-extensions enable material-shell@papyelgringo

# Add support for libsndio.so.6.1
sudo apt install -yy libsndio7.0
cd /usr/lib/x86_64-linux-gnu/
sudo ln -s libsndio.so.7.0 libsndio.so.6.1

# Add required i386 libraries to support some 32bit apps and games
sudo apt install -yy libatk-adaptor:i386 libglu1-mesa:i386 libxcursor1:i386 libasound2:i386 libasound2-plugins:i386 libcanberra-gtk-module

# Ricing
curl -fsSL https://starship.rs/install.sh | bash
echo 'eval "$(starship init bash)"' >> ~/.bashrc
echo 'neofetch' >> ~/.bashrc
sudo apt install -yy powerline fonts-powerline tmux-themepack-jimeh vim-airline
#sudo apt install -y variety
cd ~
wget https://github.com/Peltoche/lsd/releases/download/0.19.0/lsd_0.19.0_amd64.deb
sudo gdebi -n ./lsd_0.19.0_amd64.deb
rm ./lsd_0.19.0_amd64.deb
#
# Nord theme for gnome-terminal
cd ~
git clone https://github.com/arcticicestudio/nord-gnome-terminal.git
cd nord-gnome-terminal/src
./nord.sh
cd ~
rm -Rf ~/nord-gnome-terminal
# Add some Geany color schemes
cd ~
git clone https://github.com/geany/geany-themes
cd geany-themes
./install.sh
cd ~
rm -Rf ~/geany-themes

# Flat-remix themes and icons
#sudo add-apt-repository ppa:daniruiz/flat-remix
#sudo apt update
#sudo apt install -yy flat-remix flat-remix-gnome flat-remix-gtk
#gsettings set org.gnome.shell.extensions.user-theme name "Flat-Remix"
#gsettings set org.gnome.desktop.interface icon-theme "Flat-Remix-Red"
#gsettings set org.gnome.desktop.interface gtk-theme "Flat-Remix-GTK-Red"

# A script to install/update Suru++ icon theme after this you can run suru-update.sh
#wget -qO- https://raw.githubusercontent.com/gusbemacbe/suru-plus/master/install.sh | sh
#gsettings set org.gnome.desktop.interface icon-theme "Suru++"
#
# Get the script and change  Suru++ Folders to RED
#wget -qO- https://git.io/fhQdI | sh
#suru-plus-folders -C red --theme Suru++

# Download and install default NERD fonts replacements from https://github.com/ryanoasis/nerd-fonts/releases/tag/v2.0.0
cd ~
mkdir ~/.fonts
cd ~/.fonts
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Ubuntu.zip
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/UbuntuMono.zip
unzip -o ./Ubuntu.zip
unzip -o ./UbuntuMono.zip
rm ./Ubuntu.zip
rm ./UbuntuMono.zip
cd ~

# Install Pop! Shell
cd ~
git clone https://github.com/pop-os/shell.git
cd shell
make local-install
# disable hot-keys for Pop! Shell launcher compatibility
gsettings set org.gnome.shell.extensions.dash-to-dock hot-keys false
gsettings set org.gnome.shell.keybindings switch-to-application-1 "['']"
gsettings set org.gnome.shell.keybindings switch-to-application-2 "['']"
gsettings set org.gnome.shell.keybindings switch-to-application-3 "['']"
gsettings set org.gnome.shell.keybindings switch-to-application-4 "['']"
gsettings set org.gnome.shell.keybindings switch-to-application-5 "['']"
gsettings set org.gnome.shell.keybindings switch-to-application-6 "['']"
gsettings set org.gnome.shell.keybindings switch-to-application-7 "['']"
gsettings set org.gnome.shell.keybindings switch-to-application-8 "['']"
gsettings set org.gnome.shell.keybindings switch-to-application-9 "['']"


#Clean up and finish
#
sudo apt autoclean
sudo apt autoremove -yy
sudo snap refresh
#rm -f ~/initialize.sh
#rm -f ~/scripts/initialize.sh

echo "All done! Reboot the machine to get the full experience"
sleep 3
rm -f ~/scripts/deploy.sh
exit
