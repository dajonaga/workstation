#!/bin/bash
# Install Virtualization and Orquestration tools
# Set BASH to quit script and exit on errors:
set -eu

# Install Vagrant and Virtualbox
sudo apt install -yy bridge-utils vagrant virtualbox gnome-boxes
sudo snap install lxd
#sudo snap install multipass

# Initialize LXD this is an interactive process. We can go with the default values
lxd init

# Launch the LXC container and make sure it's up to date
lxc launch ubuntu:18.04 docker -c security.nesting=true
lxc exec docker -- apt update
lxc exec docker -- apt full-upgrade

# Install Docker.io https://www.docker.com/products/container-runtime https://www.ubuntuupdates.org/package/core/eoan/universe/base/docker.io
lxc exec docker -- apt install docker.io -y
lxc exec docker -- sudo systemctl enable docker

# Install Portainer for easy container management https://www.portainer.io/
lxc exec docker -- docker volume create portainer_data
lxc exec docker -- docker run -d -p 8000:8000 -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

# Limit the resources our docker LXC container is allowed to get from the host
lxc config set docker limits.cpu.allowance 5%
lxc config set docker limits.cpu.priority 0
lxc config set docker limits.memory 1GB
lxc config set docker limits.memory.swap.priority 0
lxc config set docker limits.memory.enforce soft
lxc restart docker

# HERE BE DRAGONS!
# THE FOLLOWING WILL HELP YOU CREATE A BRIDGE ON THE HOST
# AND ALLOW THE CONTAINERS TO GET AN IP ADDRESS FROM LOCAL DHCP SERVER (LAN)
# IF YOU DON'T KNOW WHAT YOU'RE DOING READ THE FOLLOWING:
#
# https://webby.land/2018/04/27/bridging-under-ubuntu-18-04/
# https://ubuntu.com/blog/converting-eth0-to-br0-and-getting-all-your-lxc-or-lxd-onto-your-lan
# http://containerops.org/2013/11/19/lxc-networking/
#
# USE THE FOLLOWING AS A TEMPLATE - CHANGE 'enp4s0' TO MATCH YOUR HOST'S NIC
# to find out your NIC name do:
#ip a
#
# sudo vi /etc/netplan/01-network-manager-all.yaml
#-----------------------------------------------------------------------
## Let NetworkManager manage all devices on this system
#network:
#  version: 2
#  renderer: NetworkManager
#  ethernets:
#    enp4s0:
#      dhcp4: no
#      dhcp6: no
#      accept-ra: no
#      
#  bridges:
#    br0:
#      accept-ra: no
#      interfaces: [enp4s0]
#      dhcp4: yes
#      dhcp6: yes
#      parameters:
#        stp: false
#-----------------------------------------------------------------------
#
#sudo netplan generate
#sudo netplan apply
# Validate that br0 gets an IP address from your LAN DHCP server
#ip a
#
# Change the default profile to assing br0 as the default for your LXC containers
#lxc profile edit default
# change 'lxdbr0' to 'br0'
#lxc restart docker
#lxc list
# Check that the container 'docker' get's an IP address from your LAN DHCP server. Should be in the same IP space as your HOST.
#
# Set up our Vagrant+Virtualbox working folder
cd ~
mkdir ubuntu-vm
cd ubuntu-vm
wget https://gitlab.com/dajonaga/workstation/raw/master/virtualization/Vagrantfile
# Uncomment the next line to auto-deploy the new VM
#vagrant up
cd ~

echo "ALL DONE!"
echo "Review the 'Vagrantfile' inside the 'virtualization' folder and modify as needed."
exit
