#!/bin/bash
# SCRIPT TO AUTOMATE INSTALLING UBUNTU DESKTOP
# Install Ubuntu Desktop (minimal installation)
# INTERNET CONNECTION NEEDED TO FULLY EXECUTE THIS PROCEDURE

cd ~

# FIRST make sure everything is up to date
sudo apt update && sudo apt full-upgrade -y

# SECOND Install git, clone the repo and set all up to execute deploy.sh
sudo apt install git -y
git clone https://gitlab.com/dajonaga/workstation.git
cd workstation/
mv ./scripts ~
cp ./dotfiles/.* ~
cp -R ./dotfiles/.config ~
cd ~
chmod +x ./scripts/*
rm -Rf ./workstation
source ~/.profile

echo "All done! Please execute source ~/.profile and run deploy.sh when ready"
