#!/bin/bash
# Set BASH to quit script and exit on errors:
set -eu

# Ubuntu/Debian system updates and clean
sudo apt update
sudo apt full-upgrade -yy
sudo apt autoclean
sudo apt autoremove -yy

# Removes old revisions of snaps for a Spanish OS install
# CLOSE ALL SNAPS BEFORE RUNNING THIS!!
echo "Refreshing all snaps, please wait..."
sudo snap refresh
echo "Removing old snaps..."
snap list --all | awk '/desactivado/{print $1, $3}' |
	while read snapname revision; do
		sudo snap remove "$snapname" --revision="$revision"
	done
echo "done!"
exit
