# Workstation

<div align='center'>
	<a href='https://raw.gitlab.com/dajonaga/workstation/assets/screenshot.png'>
		<img src='./assets/screenshot.png' />
	</a>
</div>

## Here be dragons!

I'm currently running OpenSUSE Tumbleweed. This repo is deprecated. I'll update as time and life permits. Thanks for understanding.

Personal scripts and dot files to automate and rice Ubuntu Desktop distro.

### IF YOU DON'T KNOW WHAT YOU'RE DOING, PLEASE DO SOME RESEARCH AND COME BACK LATER!!!

Currently running on Ubuntu 20.10 Desktop.

You will find 'initialize.sh' under /scripts. It is the only file needed to get this rolling.

Flat Remix's ricing is only recomended for advanced users.

Wallpaper : https://www.wallpaperflare.com/digital-digital-art-artwork-illustration-abstract-neon-wallpaper-gjjgo

Have fun!!

## Folders

Folder           | Description
-----------------|---------------------------------------------------------------------
`assets`         | non essential assets for this project (screeshots and such)
`dotfiles`       | home directory dotfiles
`scripts`        | scripts to automate a few things, like deploying a new workstation
`virtualization` | for now a Vagrantfile to automate my Ubuntu VM and deprecated scripts

## Info

### Basic

Category  | Implementation
----------|---------------------------------------------------------------------------------------------
OS        | [Ubuntu Desktop] (https://ubuntu.com/desktop)
DE/WM     | [GNOME/Mutter] (https://www.gnome.org/)
Shell     | [Bash] (https://www.gnu.org/software/bash/)
Terminal  | [Terminology] (https://www.enlightenment.org/about-terminology.md) and [tmux] (https://tmux.github.io/)
Editor    | [Vim] (https://www.vim.org/) and [Gedit] (https://wiki.gnome.org/Apps/Gedit)
Browser   | [Firefox] (https://www.mozilla.org/en-US/firefox/)
Tiling    | [Pop! Shell GNOME extension] (https://github.com/pop-os/shell)

### Theme

Category     | Implementation
-------------|-------------------------------------------------------------
Font         | [Ubuntu Nerd Font] (https://github.com/ryanoasis/nerd-fonts)
GTK Theme    | [Flat Remix] (https://github.com/daniruiz/Flat-Remix-GTK) Made optional in latest script
Color scheme | [Flat Remix Red Dark] (https://github.com/daniruiz/Flat-Remix-GTK) Made optional in latest script
Icons        | [Flat-remix] (https://github.com/daniruiz/flat-remix) Made optional in latest script
Shell prompt | [Starship] (https://starship.rs/) [Powerline] (https://powerline.readthedocs.io/en/master/)

### Other

Category              | Implementation
----------------------|---------------------------------------------------
Resource monitor      | [Bpytop] (https://github.com/aristocratos/bpytop)
Wallpaper manager     | [Variety] (https://github.com/varietywalls/variety) Made optional in latest script
Backup                | [Déjà Dup] (https://wiki.gnome.org/Apps/DejaDup)
Music                 | [Spotify desktop client] (https://www.spotify.com/)
Email                 | [Protonmail web client] (https://protonmail.com/)
Calendar              | [Protoncalendar web client] (https://protonmail.com/)
File manager          | [GNOME Files/Nautilus] (https://wiki.gnome.org/action/show/Apps/Files)
IDE                   | [Geany] (https://www.geany.org/)
Office suite          | [LibreOffice] (https://www.libreoffice.org/)

### Virtualization

Category          | Implementation
------------------|-------------------------------------------------------------
Containers        | [LXC] (https://linuxcontainers.org/) and [Docker] (https://www.docker.com/)
Virtual Machines  | [Virtualbox] (https://www.virtualbox.org/)
Container Manager | [LXD] (https://linuxcontainers.org/) and [Portainer] (https://www.portainer.io/)
Orchestation      | [Vagrant] (https://www.vagrantup.com)
