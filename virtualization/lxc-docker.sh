#!/bin/bash
# INSTALL SCRIPT TO AUTOMATE INSTALLING AN LXC Ubuntu 18.04 LTS CONTAINER RUNNING DOCKER.IO

sudo apt install bridge-utils

# Automating the creation of a bridge interface THIS IS THE PREFERED WAY
#
# https://www.cyberciti.biz/faq/ubuntu-20-04-add-network-bridge-br0-with-nmcli-command/
#
#sudo nmcli con add ifname br0 type bridge con-name br0
#sudo nmcli con add type bridge-slave ifname enp4s0 master br0
#sudo nmcli con modify br0 bridge.stp yes
# Validation
#nmcli connection show
#nmcli -f bridge con show br0
#
# Bring the interface up
#sudo nmcli con up br0
#nmcli con show
#
# HERE BE DRAGONS!
# IF THE AUTOMATION DOES NOT WORK, BELOW IS AN ALTERNATIVE WAY TO CREATE br0 INTERFACE
# THE FOLLOWING IS TO CREATE A BRIDGE ON THE HOST AND ALLOW THE CONTAINERS TO DHCP FROM LOCAL LAN
# IF YOU DON'T KNOW WHAT YOU'RE DOING READ THE FOLLOWING:
#
# https://webby.land/2018/04/27/bridging-under-ubuntu-18-04/
# https://ubuntu.com/blog/converting-eth0-to-br0-and-getting-all-your-lxc-or-lxd-onto-your-lan
# http://containerops.org/2013/11/19/lxc-networking/
#
# br0 BRIDGE INTERFACE SETUP
#
# USE THE FOLLOWING AS A TEMPLATE - CHANGE 'enp4s0' TO MATCH YOUR HOST'S NIC
# to find out your NIC name run:
#
#   ip a
#
# sudo vi /etc/netplan/01-network-manager-all.yaml
## Let NetworkManager manage all devices on this system
#network:
#  version: 2
#  renderer: NetworkManager
#  ethernets:
#    enp4s0:
#      dhcp4: no
#      dhcp6: no
#      accept-ra: no
#      
#  bridges:
#    br0:
#      accept-ra: no
#      interfaces: [enp4s0]
#      dhcp4: yes
#      dhcp6: yes
#      parameters:
#        stp: false
#EOF
#
# APPLY THE CHANGES
#   sudo netplan generate
#   sudo netplan apply
#
# VALIDATE that br0 gets an IP address from your LAN DHCP server
#
#   ip a
#
# END OF br0 BRIDGE INTERFACE SETUP

# Install LXD

sudo snap install lxd

# Initialize LXD this is an interactive process. We can go with the default values
# For easier documentation after the initial install here are some recommended changes
# to the default config:
#
# -create a new zpool and name it 'lxdpool'
# -DO NOT CREATE A NEW BRIDGE INTERFACE = ANSWER NO
# -Let LXD use the br0 interface we created above. So choose 'br0'
# 
lxd init

# Launch a new LXC container and make sure it's up to date
lxc launch ubuntu:20.04 docker -c security.nesting=true
lxc exec docker -- apt update
lxc exec docker -- apt full-upgrade -y
lxc exec docker -- apt autoremove -y
# Limit the resources our docker LXC container is allowed to get from the host
lxc config set docker limits.cpu.allowance 5%
lxc config set docker limits.cpu.priority 0
lxc config set docker limits.memory 1GB
lxc config set docker limits.memory.swap.priority 0
lxc config set docker limits.memory.enforce soft
lxc restart docker

# Install Docker.io https://www.docker.com/products/container-runtime https://www.ubuntuupdates.org/package/core/eoan/universe/base/docker.io
lxc exec docker -- apt install docker.io -y
lxc exec docker -- sudo systemctl enable docker
lxc restart docker

# Install Portainer for easy container management https://www.portainer.io/
lxc exec docker -- docker volume create portainer_data
lxc exec docker -- docker pull portainer/portainer-ce
lxc exec docker -- docker run -d -p 8000:8000 -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce

# To update portainer to the latest run the following
#lxc exec docker -- docker stop portainer
#lxc exec docker -- docker rm portainer
#lxc exec docker -- docker pull portainer/portainer-ce
#lxc exec docker -- docker run -d -p 8000:8000 -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
#lxc exec docker -- docker run --name unifi -d -p 3478:3478/udp -p 8080:8080 -p 8443:8443 -p 8880:8880 -p 8843:8843 goofball222/unifi

# IF NOT DONE ABOVE THEN:
# Change the default profile to assing br0 as the default for your LXC containers
#lxc profile edit default
# change 'lxdbr0' to 'br0'
#lxc restart docker
#lxc list
# Check that the container 'docker' get's an IP address from your LAN DHCP server. Should be in the same IP space as your HOST.
