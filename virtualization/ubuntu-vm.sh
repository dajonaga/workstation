#!/bin/bash
# Set BASH to quit script and exit on errors:
set -eu

# Clone the repo and prepare the working folder
cd ~
git clone https://gitlab.com/dajonaga/workstation.git
mkdir ubuntu-vm
cd ubuntu-vm
wget https://releases.hashicorp.com/vagrant/2.2.6/vagrant_2.2.6_x86_64.deb
wget https://download.virtualbox.org/virtualbox/6.1.0/virtualbox-6.1_6.1.0-135406~Ubuntu~eoan_amd64.deb
cp ~/workstation/ubuntu-vm/*.rb ./
cp ~/workstation/ubuntu-vm/Vagrantfile ./

# Install Vagrant and Virtualbox
sudo gdebi -n vagrant_2.2.6_x86_64.deb
sudo gdebi -n virtualbox-6.1_6.1.0-135406~Ubuntu~eoan_amd64.deb

# Patch Vagrant 2.2.6 to add support for Virtualbox 6.1
sudo cp ./meta.rb /opt/vagrant/embedded/gems/2.2.6/gems/vagrant-2.2.6/plugins/providers/virtualbox/driver/
sudo cp ./version_6_1.rb /opt/vagrant/embedded/gems/2.2.6/gems/vagrant-2.2.6/plugins/providers/virtualbox/driver/
sudo cp ./plugin.rb /opt/vagrant/embedded/gems/2.2.6/gems/vagrant-2.2.6/plugins/providers/virtualbox/

# Cleaning up
rm -f ~/ubuntu-vm/*.rb
rm -f ~/ubuntu-vm/*.deb
rm -Rf ~/workstation

# Uncomment the next line to deploy the new VM
#vagrant up
echo "ALL DONE!"
echo "Review the 'Vagrantfile' and modify as needed. Run 'vagrant up' when ready."
exit
